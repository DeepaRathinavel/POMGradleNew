package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{	
	@CacheLookup
	@FindBy(xpath= "//input[@id=\"ext-gen248\"]") 
	WebElement eleFirstName;


	public FindLeadPage typeFirstName(String data) {
		//	WebElement eleFirstName = locateElement("xpath", "//input[@id=\"ext-gen248\"]");
		type(eleFirstName, data);
		return this;
	}

	@CacheLookup
	@FindBy(xpath= "//button[text()=\"Find Leads\"]") 
	WebElement eleFindLead;

	public FindLeadPage clickFindLeadbutton() throws InterruptedException {			
		//	WebElement eleFindLead= locateElement("xpath", "//button[text()=\"Find Leads\"]");
		click(eleFindLead);
		Thread.sleep(3000);
		return this; 

	}

	/*@CacheLookup
		@FindBy(xpath= "//div[@class=\"x-grid3-body\"]/div[1]/table/tbody/tr[1]/td[1]/div/a") 
		//WebElement eleFirstLead;
	 */		
	public ViewLeadPage clickFirstLead() throws InterruptedException {

		WebElement eleFirstLead= locateElement("//div[@class=\"x-grid3-body\"]/div[1]/table/tbody/tr[1]/td[1]/div/a");	

		return new ViewLeadPage(); 
	}

	public FindLeadPage typeEmail(String data) {
		WebElement eleEmailName = locateElement("xpath", "//input[@name='emailAddress']");
		type(eleEmailName, data);
		return this;
	}

	public FindLeadPage clickEmail() {
		WebElement eleEmailName = locateElement("xpath", "//*[text()='Email']");
		click(eleEmailName);
		return this;
	}
	
	public FindLeadPage clickFindLeadEmailbutton() throws InterruptedException {			
		WebElement eleFindLead= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLead);
		Thread.sleep(3000);
		return this; 

	}
	
	public DuplicateLeadPage clickFirstEmailRow() throws InterruptedException {

		WebElement eleFirstLead= locateElement("(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[1]/a");	

		return new DuplicateLeadPage(); 
	}
	
	public FindLeadPage clickPhoneTab() throws InterruptedException {			
		WebElement elePhoneTab= locateElement("xpath", "//ul[@class=\"x-tab-strip x-tab-strip-top\"]/li[2]/a[2]");
		click(elePhoneTab);
		Thread.sleep(3000);
		return this; 

	}
	
	public FindLeadPage typePhoneNumber(String data) {
		WebElement elePhoneNumber = locateElement("xpath", "//input[starts-with(@name,\"phoneAreaCode\")]");
		type(elePhoneNumber, data);
		return this;
	}
	
	public FindLeadPage clickFindLeadPhoneTab() throws InterruptedException {			
		WebElement eleFindLead= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLead);
		Thread.sleep(3000);
		return this; 

	}
	
	public ViewLeadPage clickFirstPhoneRow() throws InterruptedException {

		WebElement eleFirstLead= locateElement("//div[@class=\"x-grid3-body\"]/div[1]/table/tbody/tr[1]/td[1]/div/a");	

		return new ViewLeadPage(); 
	}
	
}









