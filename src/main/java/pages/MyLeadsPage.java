package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	/*@CacheLookup
	@FindBy(linkText= "Find Leads")
	WebElement eleFindLead; */
	
	public FindLeadPage clickFindLead() {
		WebElement eleFindLead = locateElement("linktext", "Find Leads");
		click(eleFindLead);
		return new FindLeadPage();
	}
	
	/*@CacheLookup
	@FindBy(linkText= "Create Lead")
	WebElement eleCreateLead; */
	
	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	/*@CacheLookup
	@FindBy(linkText= "Merge Leads")
	WebElement eleMergeLead; */
	
	public MergeLeadsPage clickMergeLead() {
		WebElement eleMergeLead = locateElement("linktext", "Merge Leads");
		click(eleMergeLead);
		return new MergeLeadsPage();
	}
}









