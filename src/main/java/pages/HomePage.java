package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	/*@CacheLookup
	@FindBy(linkText= "CRM/SFA") 
	WebElement eleCRMSFA;*/
	
	public MyHomePage clickCRMSFA() {
		WebElement eleCRMSFA = locateElement("linktext", "CRM/SFA");
		click(eleCRMSFA);
		return new MyHomePage();
	}
	
}









