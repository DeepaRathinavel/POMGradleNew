package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {
	/*@CacheLookup
	@FindBy(id= "updateLeadForm_companyName") 
	WebElement eleCompanyName;
	
	@CacheLookup
	@FindBy(xpath= "//input[@value=\"Update\"]")
	WebElement eleUpdateLead;*/
	
	public EditLeadPage typeCompanyName(String data) throws InterruptedException {
		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");		
		type(eleCompanyName, data);
		Thread.sleep(3000);
		return this;
	}
	
	public EditLeadPage clickUpdateLead() throws InterruptedException {
		WebElement eleUpdateLead= locateElement("xpath", "//input[@value=\"Update\"]");
		Thread.sleep(3000);
		click(eleUpdateLead);
		return this; 
	}
	
	
	
}
