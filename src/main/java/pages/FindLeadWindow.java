package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class FindLeadWindow extends ProjectMethods {

	/*@CacheLookup
	@FindBy(xpath= "//input[@name=\"id\"]")
	WebElement eleLeadId;*/
	//switchToWindow(1);
	//getTitle();
	
	public FindLeadWindow typeLeadId(String data) {
		WebElement eleLeadId = locateElement("xpath", "//input[@name=\"id\"]");
		type(eleLeadId, data);
		return this;
	}
	
/*	@CacheLookup
	@FindBy(xpath= "//button[text()=\"Find Leads\"]")
	WebElement eleFindLead;*/
	
	public FindLeadWindow clickFindLead() {
		WebElement eleFindLead= locateElement("xpath", "//button[text()=\"Find Leads\"]");
		click(eleFindLead);
		return this; 
	}
	
	/*@CacheLookup
	@FindBy(xpath= "//table[@class=\"x-grid3-row-tabl\"]/tbody/tr[1]/td[1]/div/a")
	WebElement eleFirstLead;*/
	
	public MergeLeadsPage clickFirstLead() throws InterruptedException {
		WebElement eleFirstLead= locateElement("xpath", "//table[@class=\"x-grid3-row-table\"]/tbody/tr[1]/td[1]/div/a");
		Thread.sleep(3000);
		click(eleFirstLead);
		
		switchToWindow(0);
		return new MergeLeadsPage(); 
	}

}
