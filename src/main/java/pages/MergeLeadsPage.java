package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;


public class MergeLeadsPage extends ProjectMethods {
	
		
		/*@CacheLookup
		@FindBy(xpath= "//img[@src=\"/images/fieldlookup.gif\"]")
		WebElement eleFromLead;*/
		
		
		public FindLeadWindow clickFromLead() throws InterruptedException {
			
			WebElement eleFromLead = locateElement("//img[@src=\"/images/fieldlookup.gif\"]");
			///click(eleFromLead);
			Thread.sleep(3000);
			switchToWindow(1);
			return new FindLeadWindow();
		}
				
		/*@CacheLookup
		@FindBy(xpath= "//a[@class=\"buttonDangerous\"]")
		WebElement eleMergeLead; */
		
	public FindLeadWindow clickMergeLead() throws InterruptedException {
		WebElement eleMergeLead= locateElement("xpath","//a[@class=\"buttonDangerous\"]");
		click(eleMergeLead);
		acceptAlert();		
		Thread.sleep(3000);
		//WebElement errMsgLoc = locateElement("xpath","//div[@class=\"errorMessageHeader\"]");
	   // verifyExactText(errMsgLoc, data);
		return new FindLeadWindow(); 
	}

	}
