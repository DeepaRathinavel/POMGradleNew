package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;
public class DuplicateLeadPage extends ProjectMethods {
	public DuplicateLeadPage clickCreateLead() {
		WebElement eleCreateLead= locateElement("LinkText", "Create Lead");
		click(eleCreateLead);
		return this; 
	}
}
