package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
/*	@CacheLookup
	@FindBy(linkText= "Find Leads")
	WebElement eleFindLead;*/ 
	
	public EditLeadPage viewLeadPage() throws InterruptedException {
		Thread.sleep(3000);
		WebElement eleCreateLead= locateElement("//div[@class=\"frameSectionExtra\"]/a[3]");
		//click(eleCreateLead);
		
		return new EditLeadPage(); 
	}
	
	public EditLeadPage duplicateLeadPage() throws InterruptedException {
		Thread.sleep(3000);
		WebElement eleCreateLead= locateElement("//a[text()='Duplicate Lead']");
		//click(eleCreateLead);
		
		return new EditLeadPage(); 
	}
	
	public MyHomePage clickDeleteButton() throws InterruptedException {
		Thread.sleep(3000);
		WebElement eleDeleteLead= locateElement("//a[text()=\"Delete\"]");
		//click(eleCreateLead);
			
		return new MyHomePage(); 
	}
}
