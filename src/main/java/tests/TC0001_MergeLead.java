package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC0001_MergeLead extends ProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public  void mergeLead(String toLead) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickMergeLead()
		.clickFromLead()
		.typeLeadId(toLead)
		.clickFindLead()
		.clickFirstLead()
		.clickMergeLead();	
	}
}
