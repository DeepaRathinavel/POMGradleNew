package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC001_DuplicateLead extends ProjectMethods {

	
		@BeforeClass
		public void setData() {
			testCaseName = "TC001_CreateLead";
			testCaseDescription ="Create a lead";
			category = "Smoke";
			author= "Babu";
			dataSheetName="TC004";
		}
		@Test(dataProvider="fetchData")
		public  void duplicateLead(String email) throws InterruptedException   {
			new MyHomePage()
			.clickLeads()	
			.clickFindLead()
		 .clickEmail()
		  .typeEmail(email)
		  .clickFindLeadEmailbutton()
		  .clickFirstEmailRow()
		  .clickCreateLead();
			
			//Duplicate Lead | opentaps CRM
		}
		
	}
