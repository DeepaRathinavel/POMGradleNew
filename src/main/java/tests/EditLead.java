package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="EditLead";
	}
	@Test(dataProvider="fetchData")
	public  void UpdateLead(String firstName, String CName) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()	
		.clickFindLead()
		.typeFirstName(firstName)
		.clickFindLeadbutton()
		.clickFirstLead()
		.viewLeadPage()
		.typeCompanyName(CName)
		.clickUpdateLead();
		
}
}
